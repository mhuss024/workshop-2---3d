#include "ofApp.h"

//--------------------------------------------------------------

//int numCols = 50;
//int numRows = 50;
//int spacing = 1;


void ofApp::setup(){
    n1_freq = 0.1;
    n2_amp = 0.1;
    n1_amp = 2;
    n2_amp = 2;
    
    framenumber = 0;
    ofEnableDepthTest();
    cam.setPosition(-10,-100, 20);
    cam.lookAt(glm::vec3(10,10,20),glm::vec3(0,0,1));
    box = ofBoxPrimitive(10,10,10);
    box.setPosition(0, 0, 5);
    int numCols = 50;
    int numRows = 50;
    int spacing = 10;
    
    
    for (int i = -numCols/2 ; i<numCols/2 ; i++){
        for(int j = -numRows/2; j< numRows/2; j++){
            mesh.addVertex(ofPoint(i,j,getZNoiseValue(i,j,framenumber)));
            mesh.addColor(ofColor(255,0,0));
        }
  
}
    mesh.setMode(OF_PRIMITIVE_TRIANGLES);
    for(int i = 0; i < numCols - 1; i++) {
        for(int j = 0; j < numRows - 1; j++) {
            int topLeft     = i   + numCols * j;
            int bottomLeft  = i+1 + numCols * j;
            int topRight    = i   + numCols * (j+1);
            int bottomRight = i+1 + numCols * (j+1);

            mesh.addTriangle( topLeft, bottomLeft, topRight);
            mesh.addTriangle( bottomLeft, topRight, bottomRight);

        }
    }
}
float ofApp::getZNoiseValue(float x, float y, int framenumber){
    float waves = framenumber*0.02;
    float n1 = ofNoise(x * n1_freq ,y * n1_freq ,waves) * n1_amp;
    float n2 = ofNoise(x * n2_freq ,y * n2_freq ,waves) * n2_amp;
    return n1 + n2;
    
}

//--------------------------------------------------------------
void ofApp::update(){
    for (int w = 0; w <mesh.getNumVertices();w++){
        ofVec3f dir;
        dir = mesh.getVertex(w);
        dir.z = getZNoiseValue(dir.x, dir.y, framenumber);
        mesh.setVertex(w, dir);
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofSetColor(25, 0, 51);
     cam.begin();
    mesh.drawWireframe();
    framenumber = framenumber + 1;
    box.draw();
     cam.end();
    
    
    
    //dolly(...)void ofNode::dolly(float amount)
    //Move forward+backward (in local z axis)
    


}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if (key== 'w'){
        cam.dolly(10);
        //the box moves backwards
    }
    if (key == 'a'){
    cam.dolly(-10);
        //the box moves forward
    }
    if (key == 's'){
        cam.panDeg(10);
        //box moves to the left
    }
    if (key == 'd'){
        cam.panDeg(-10);
        //box moves to the right
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
